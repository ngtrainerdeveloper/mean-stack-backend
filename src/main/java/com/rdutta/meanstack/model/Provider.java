package com.rdutta.meanstack.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Provider implements Serializable {
    
    @JsonIgnore
    @Transient
	int Min = 100000;
	@JsonIgnore
    @Transient
	int Max = 999999;
    @Id
    @Column(updatable = false)
    private Long id = (long) Math.floor(Math.random() * (Max - Min) + Min);
    private String firstname;
    private String lastname;
    private String position;

    @OneToOne(targetEntity = Company.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "pc_fk", referencedColumnName = "id")
    private Company company;

}
