package com.rdutta.meanstack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeanstackApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeanstackApplication.class, args);
	}

}
